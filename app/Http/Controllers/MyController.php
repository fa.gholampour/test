<?php


namespace App\Http\Controllers;



use App\one\Domain\Categories\Service\CategoryService;

class MyController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * MyController constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function categoriesList()
    {

        $this->categoryService->getList();
    }

    public function add()
    {
        $this->categoryService->create();
    }

}